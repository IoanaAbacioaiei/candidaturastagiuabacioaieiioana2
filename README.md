Aceasta este o aplicatie care calculeaza salariul angajatilor dintr-o firma, in functie de numarul de ore lucrate si suma platita pe ora. In plus, aceasta determina si salariul maxim. In realizarea proiectului, am folosit o clasa, numita Person, care contine numele, prenumele si numarul de ore lucrate de angajat.

 Metodele utilizate sunt: 

-constructor fara argumente/constructor cu argumente pentru initializarea campurilor aferente.

-functii de returnare a numelui, prenumelui si a numarului de ore. Datele utilizate sunt private, deci accesul la ele se realizeaza prin intermediul acestor functii.

-functie de afisare a datelor (public String toString()).

-functie de determinare a salariului in functie de numarul de ore lucrate si de suma platita pe ora (public double getPayment(int HourlyRate))

Citirea datelor se realizeaza din fisierul ”Person.txt”
Acesta contine datele: prenume, nume, numarul de ore lucrate

Citirea datelor este construita in clasa “CustomFileReader”, iar apelarea si determinarea datelor de iesire se realizeaza in Main. 

Format afisare date de iesire: 
Person--->>> FirstName:.........., LastName:..........., Hours:.....
Payment:.............
MaxPayment: ........