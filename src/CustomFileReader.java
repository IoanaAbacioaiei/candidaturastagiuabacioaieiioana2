import java.io.*;
import java.util.*;

public class CustomFileReader {
	private Scanner scanner;

	private void openFile() throws FileNotFoundException {
			scanner = new Scanner(new File("Person.txt"));
	}

	/**
	 * 
	 * @return list of persons from file
	 * @throws FileNotFoundException
	 */
	public List<Person> readPersons() throws FileNotFoundException {
		this.openFile();
		List<Person> persons = new ArrayList<Person>();	
		while (scanner.hasNext()) {
			Person person = new Person();
			person.setFirstName(scanner.next());
			person.setLastName(scanner.next());
			person.setHourlyRate(Double.parseDouble(scanner.next()));
			persons.add(person);
		}
		this.closeFile();
		return persons;
	}

	private void closeFile() {
		scanner.close();
	}

}
