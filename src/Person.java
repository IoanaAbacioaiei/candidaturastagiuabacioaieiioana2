
/**
 * This class has necessary fields for representing a person.
 */
public class Person {

	private String FirstName;
	private String LastName;
	private double Hours;

	public Person() {
		this.FirstName = " ";
		this.LastName = " ";
		this.Hours = 0;		
	}
	
	public Person(String firstName, String lastName, double hourlyRate) {
		FirstName = firstName;
		LastName = lastName;
		Hours = hourlyRate;
	}
	
	public String getFirstName() {
		return FirstName;
	}
	
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}
	
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	
	public double getHours() {
		return Hours;
	}

	public void setHourlyRate(double hours) {
		Hours = hours;
	}
	
	@Override
	public String toString() {
		return "Person--->>> FirstName:" + FirstName + ", LastName:" + LastName + ", Hours:" + Hours + " ";
	}
	
	public double getPayment(int HourlyRate){
		return this.Hours*HourlyRate;
	}
	


	
	
	
}
