import java.io.FileNotFoundException;
import java.util.List;

public class Main {
	
	public static void main(String[] args){
		double max = 0;
		CustomFileReader cfr = new CustomFileReader();
		try {
			List<Person> persons = cfr.readPersons();
			for(Person person : persons){
				System.out.println(person);
				System.out.println("Payment: "+person.getPayment(10)+"");
				if(person.getPayment(10)>max)
				{
					max=person.getPayment(10);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.print(e);
		}
		System.out.println("MaxPayment: "+max+"");
	}
}
